--Summary of Code--

 There are two total python scripts for the two data collections (spectra and     bending). 

 Each script runs a certain git command for each sections of the sets of data     that individually plots a graph and saves an excel sheet with the name of the  element it is pulled from.

 To run spectra, type python spectra.py spectra
 To run bending (bending1.py is the correct code) type python bending1.py bending. 
 Do this while in the p2-the-gamers repository

 The git commands found in each pythons script can be found in the directory      plot_trial 


--Assumptions for Code to Work for New Users--

 The assumptions that this code is based off of is that they will be working with the same elements and save them in the same order as portraying in the spectra  and bending directories, otherwise the names will be wrong and confused.

--Output of the Code for Comparison to New Users--

 The output of this code will show 5 plots one after the other, which will save   as a .png and as an excel sheet after being closed. After being closed, it will show the next plot and on and on.

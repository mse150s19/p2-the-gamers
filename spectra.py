import matplotlib.pyplot
import glob
import os
import pandas as pd
import sys
import numpy
import xlsxwriter  

directory = sys.argv[1]

if  directory == 'spectra':
    files = directory+'/Sp*'
    filenames = sorted(glob.glob(files))
    for f in filenames:
        file_nlist=[i for i in f.split('_')]
        file_list = file_nlist[-1].split('.')[0]

        data = numpy.loadtxt(f,delimiter="\t")
        df = pd.DataFrame(data)
        filepath = file_list+'.xlsx'
        df.to_excel(filepath, index=False)

        fig = matplotlib.pyplot.figure(figsize=(10.0,3.0))

        axes1 = fig.add_subplot(1, 3, 1)
        axes1.set_ylabel('Stress (Pa)')
        axes1.set_xlabel('Strain')
        axes1.plot(data[:,0],data[:,1])

        fig.tight_layout()
        matplotlib.pyplot.show()




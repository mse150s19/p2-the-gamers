import matplotlib.pyplot as plt
import pandas as pd
import glob
import xlsxwriter
from io import StringIO
import sys

directory = sys.argv[1]                                                      #prints name of directory 
filenames = glob.glob('{}/*'.format(directory))                              #formats it so that it gives us a list of names within this directory

for i, f in enumerate(filenames):                                            # this outer for loops deals with the files in th directory
	path = [i for i in f.split('-')]                                     #splits the file at every "-"  
	path2 = path[-1].split('.')[0]                                       #takes the split up name and uses it to name it's file     
	filename = open(f)
	lines = filename.read().split('"Specimen"')                          #splits data in file amongst the specimens.
	for e in range (1,len(lines)):                                       #this for loop deals with the data sets with in each file
		df = pd.read_csv(StringIO(lines[e]), sep=",", skiprows=18)   #seperates values
		x= df['Strain [Exten.] %']                                   # x axis data assinged
		y= df['Stress MPa']                                          #y axis data assinged
		df2 = pd.DataFrame(x,y)                                      #takes the array to create an excel file
		plt.plot(x,y)                                                # make splot
		plt.ylabel('Stress MPa')
		plt.xlabel('Strain [Exten.] %')
		plt.show()
		fn= 'pd_{}_{}.xlsx'.format(path2,e)
		df2.to_excel(fn)                                             #creates excel sheet

          

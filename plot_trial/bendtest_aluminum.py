# This  example uses numpy, matplotlib, and xlsxwriter

import matplotlib.pyplot as plt
import xlsxwriter
import numpy as np
import sys
import functools

with open("../bending/Sp15_245L_sec-001_group-01_bendtest-aluminum.raw") as fileobj:
	f_read_ch = functools.partial(fileobj.read, 33)
	for ch in iter(f_read_ch, '15:23'):
		data = 

data = np         	        
mpl_fig = plt.figure()
ax = mpl_fig.add_subplot(111)

exit()
x, y = data[:, 3], data[:, 7]


ax.set_xlabel("Stress")
ax.set_ylabel("Strain")
ax.set_title("Aluminum Modulus of Elasticity")

plt.plot(x, y)
plt.show()

# Create excel file
workbook = xlsxwriter.Workbook('bendtest_aluminum.xlsx')
# Add a worksheet to our workbook
worksheet = workbook.add_worksheet()

# On the ith row, write x values in col 0, y values in col 1
for i, (x, y) in enumerate(zip(x, y)):
    worksheet.write(i, 0, x)
    worksheet.write(i, 1, y)

# Close and save the workbook
workbook.close()


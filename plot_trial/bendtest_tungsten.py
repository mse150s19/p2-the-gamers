# This example uses numpy, matplotlib, and xlsxwriter

import matplotlib.pyplot as plt
import xlsxwriter
import numpy as np
import sys

file_name = sys.argv[1]
data = np.genfromtxt(file_name, delimiter="\t")
mpl_fig = plt.figure()
ax = mpl_fig.add_subplot(111)

x, y = data[4], data[8]

ax.set_xlabel("Stress")
ax.set_ylabel("Strain")
ax.set_title("Tungsten Modulus of Elasticity")

plt.plot(x, y)
plt.show()

# Create excel file
workbook = xlsxwriter.Workbook('bendtest_tungsten.xlsx')
# Add a worksheet to our workbook
worksheet = workbook.add_worksheet()

# On the ith row, write x values in col 0, y values in col 1
for i, (x, y) in enumerate(zip(x, y)):
    worksheet.write(i, 0, x)
    worksheet.write(i, 1, y)

# Close and save the workbook
workbook.close()


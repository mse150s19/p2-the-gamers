# This example uses numpy, matplotlib, and xlsxwriter

import matplotlib.pyplot as plt
import xlsxwriter
import numpy as np
import sys

mpl_fig = plt.figure()
ax = mpl_fig.add_subplot(111)
ax.set_xlabel("Wavelength (nm)")
ax.set_ylabel("Intensity (a.u.)")
ax.set_title("Spectrum of H2")

file_name = sys.argv[1]
data = np.genfromtxt(file_name, delimiter="\t")

x, y = data[:, 0], data[:, 1]

plt.plot(x, y)
plt.show()
mpl_fig.savefig('H2')
# Create excel file
workbook = xlsxwriter.Workbook('spectra_data_H2.xlsx')
# Add a worksheet to our workbook
worksheet = workbook.add_worksheet()

# On the ith row, write x values in col 0, y values in col 1
for i, (x, y) in enumerate(zip(x, y)):
    worksheet.write(i, 0, x)
    worksheet.write(i, 1, y)

# Close and save the workbook
workbook.close() 
